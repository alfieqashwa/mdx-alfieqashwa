export default {
  font: 'Georgia, serif',
  lineHeight: '1.5',
  colors: {
    text: '#111',
    background: '#fff',
    link: '#07c',
  },
  LayoutSidebar: {
    backgroundColor: '#f6f6f6',
    paddingTop: '32px',
    paddingBottom: '32px',
  },
}
