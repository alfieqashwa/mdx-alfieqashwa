import styled from 'styled-components'
import theme from 'styled-theming'
import colors from '../theme/colors'

export const buttonBackgroundColor = theme('mode', {
  light: colors.lightBlue,
  dark: colors.white,
})

export const buttonColor = theme('mode', {
  light: colors.white,
  dark: colors.darkBlue,
})

const Button = styled.button`
  font-size: 1em;
  margin: 1em;
  padding: 0.25em 1em;
  border: 2px solid palevioletred;
  background-color: ${buttonBackgroundColor};
  color: ${buttonColor};
  border-radius: 3px;
  /* cursor: pointer; */
`

export default Button
