import React, { Component } from 'react';

class Counter extends Component {
  constructor(props) {
    super(props);

    this.state = { count: 0 };

    this.handleInc = this.handleInc.bind(this);
    this.handleDec = this.handleDec.bind(this);
  }

  handleInc() {
    this.setState(prevState => ({
      count: prevState + 1
    }));
  }

  handleDec() {
    this.setState(prevState => ({
      count: prevState - 1
    }));
  }

  render() {
    const { count } = this.state;
    return (
      <div>
        <h2>{count}</h2>
        <button type="button" onClick={this.handleInc}>increment</button>
        <button type="button" onClick={this.handleDec}>decrement</button>
      </div>
    );
  }
}

export default Counter;
