import React from 'react'
import Helmet from 'react-helmet'

export default () => (
  <Helmet>
    {/* <!-- Primary Meta Tags --> */}
    <title>Alfie Qashwa | Celloworld</title>
    <meta name="title" content="Alfie Qashwa | Celloworld" />
    <meta name="description" content="Learning In Public As Dead As Leaves" />

    {/* <!-- Open Graph / Facebook --> */}
    <meta property="og:type" content="website" />
    <meta property="og:url" content="https://www.alfieqashwa.me/" />
    <meta property="og:title" content="Alfie Qashwa | Celloworld" />
    <meta
      property="og:description"
      content="Learning In Public As Dead As Leaves"
    />
    <meta property="og:image" content="" />

    {/* <!-- Twitter --> */}
    <meta property="twitter:card" content="summary_large_image" />
    <meta property="twitter:url" content="https://www.alfieqashwa.me/" />
    <meta property="twitter:title" content="Alfie Qashwa | Celloworld" />
    <meta
      property="twitter:description"
      content="Learning In Public As Dead As Leaves"
    />
    <meta property="twitter:image" content="" />

    {/* <!-- Favicomatic --> */}
    {/* <link
      rel="icon"
      type="image/png"
      href="android-chrome-512x512"
      sizes="512x512"
    />
    <link
      rel="icon"
      type="image/png"
      href="android-chrome-192x192"
      sizes="192x192"
    />
    <link rel="icon" type="image/png" href="mstile-150x150" sizes="270x270" />
    <link
      rel="icon"
      type="image/png"
      href="apple-touch-icon.png"
      sizes="180x180"
    />
    <link
      rel="icon"
      type="image/svg"
      href="safari-pinned-tab"
      sizes="1118x1118"
    />
    <link rel="icon" type="image/png" href="favicon-32x32.png" sizes="32x32" />
    <link rel="icon" type="image/png" href="favicon-16x16.png" sizes="16x16" /> */}
  </Helmet>
)
