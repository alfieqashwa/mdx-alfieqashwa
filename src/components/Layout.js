// require('../utils/global.css')
// import 'prismjs/themes/prism-okaidia.css'
// import 'prismjs/plugins/line-numbers/prism-line-numbers.css'
import React from 'react'
import { Link } from 'gatsby'
import Toggle from './Toggle'
import Helmet from 'react-helmet'
import Wobble from 'react-reveal/Wobble'
import Zoom from 'react-reveal/Zoom'
import Spin from 'react-reveal/Spin'
import GlobalStyle from '../utils/GlobalStyle'
import { rhythm, scale } from '../utils/typography'
import sun from '../../content/assets/sun.png'
import moon from '../../content/assets/moon.png'

class Layout extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      theme: null,
    }
  }

  componentDidMount() {
    this.setState({ theme: window.__theme })
    window.__onThemeChange = () => {
      this.setState({ theme: window.__theme })
    }
  }
  renderHeader() {
    const { location, title } = this.props
    const rootPath = `${__PATH_PREFIX__}/`

    if (location.pathname === rootPath) {
      return (
        <h1
          style={{
            fontFamily: 'Montserrat, sans-serif',
            ...scale(0.75),
            marginBottom: 30,
            marginTop: 0,
          }}
        >
          <Link
            style={{
              boxShadow: 'none',
              textDecoration: 'none',
              color: 'var(--pink)',
            }}
            to={'/'}
          >
            <Zoom right cascade>
              {title}
            </Zoom>
          </Link>
        </h1>
      )
    } else {
      return (
        <h3
          style={{
            fontFamily: 'Montserrat, sans-serif',
            marginTop: 0,
            marginBottom: 0,
            height: 42 /* because */,
            lineHeight: '2.625rem',
          }}
        >
          <Link
            style={{
              boxShadow: 'none',
              textDecoration: 'none',
              color: 'var(--pink)',
            }}
            to={'/'}
          >
            <Zoom right cascade>
              {title}
            </Zoom>
          </Link>
        </h3>
      )
    }
  }
  render() {
    const { children } = this.props

    return (
      <div
        style={{
          color: 'var(--textNormal)',
          background: 'var(--bg)',
          transition: 'color 0.2s ease-out, background 0.2s ease-out',
          minHeight: '100vh',
        }}
      >
        <Helmet
          meta={[
            {
              name: 'theme-color',
              content: this.state.theme === 'dark' ? '#282c35' : '#ffa8c5',
            },
          ]}
        />
        <div
          style={{
            marginLeft: 'auto',
            marginRight: 'auto',
            maxWidth: rhythm(24),
            padding: `1.625rem ${rhythm(3 / 4)}`,
          }}
        >
          <header
            style={{
              display: 'flex',
              justifyContent: 'space-between',
              alignItems: 'center',
            }}
          >
            {this.renderHeader()}
            {this.state.theme !== null ? (
              <Spin>
                <Toggle
                  icons={{
                    checked: (
                      <img
                        src={moon}
                        width="16"
                        height="16"
                        role="presentation"
                        style={{ pointerEvents: 'none' }}
                      />
                    ),
                    unchecked: (
                      <img
                        src={sun}
                        width="16"
                        height="16"
                        role="presentation"
                        style={{ pointerEvents: 'none' }}
                      />
                    ),
                  }}
                  checked={this.state.theme === 'dark'}
                  onChange={e =>
                    window.__setPreferredTheme(
                      e.target.checked ? 'dark' : 'light'
                    )
                  }
                />
              </Spin>
            ) : (
              <div style={{ height: '24px' }} />
            )}
          </header>
          {children}
        </div>
        <GlobalStyle />
      </div>
    )
  }
}

export default Layout
