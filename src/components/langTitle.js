import { css } from 'styled-components'

const GatsbyHighlight = css`
  border-radius: 0 0 4px 4px;
  /* color: hsla(270, 17.119554496%, 0%, 0.92); */
  font-weight: 500;
  font-size: 0.75rem;
  font-family: SFMono-Regular, Menlo, Monaco, Consolas, Liberation Mono,
    Courier New, monospace;
  letter-spacing: 0.075em;
  line-height: 1;
  padding: 0.25rem 0.5rem;
  position: absolute;
  left: 0.3rem;
  text-align: left;
  text-transform: uppercase;
  top: 0;
`

export const langsTitle = css`
  overflow: auto;
  margin-right: -2.3125rem;
  margin-left: -2.0317rem;
  padding-right: 1em;
  padding-left: 0.75em;

  /* === PLUGIN-CODE === */
  pre[class*='language-js']::before {
    content: 'js';
    color: hsla(270, 17.119554496%, 0%, 0.92);
    background: #f7df1e;
    ${GatsbyHighlight};
  }

  pre[class*='language-javascript']::before {
    content: 'js';
    color: hsla(270, 17.119554496%, 0%, 0.92);
    background: #f7df1e;
    ${GatsbyHighlight};
  }

  pre[class*='language-jsx']::before {
    content: 'jsx';
    background: #61dafb;
    ${GatsbyHighlight};
  }
  pre[class*='language-graphql']::before {
    content: 'GraphQL';
    background: #e10098;
    color: #ffffff;
    ${GatsbyHighlight};
  }
  pre[class*='language-html']::before {
    content: 'html';
    background: #005a9c;
    color: #ffffff;
    ${GatsbyHighlight};
  }
  pre[class*='language-css']::before {
    content: 'css';
    background: #ff9800;
    color: hsla(270, 17.119554496%, 0%, 0.92);
    ${GatsbyHighlight};
  }
  pre[class*='language-shell']::before {
    content: 'shell';
    ${GatsbyHighlight};
  }
  pre[class*='language-sh']::before {
    content: 'sh';
    ${GatsbyHighlight};
  }
  pre[class*='language-bash']::before {
    content: 'bash';
    ${GatsbyHighlight};
  }
  pre[class*='language-yaml']::before {
    content: 'yaml';
    background: #ffa8df;
    ${GatsbyHighlight};
  }
  pre[class*='language-markdown']::before {
    content: 'md';
    ${GatsbyHighlight};
  }
  pre[class*='language-json']::before {
    content: 'json';
    background: linen;
    ${GatsbyHighlight};
  }
  pre[class*='language-json5']::before {
    content: 'json';
    background: linen;
    ${GatsbyHighlight};
  }
  pre[class*='language-diff']::before {
    content: 'diff';
    background: #e6ffed;
    ${GatsbyHighlight};
  }
  pre[class*='language-text']::before {
    content: 'text';
    background: #ffffff;
    ${GatsbyHighlight};
  }
  pre[class*='language-flow']::before {
    content: 'flow';
    background: #e8bd36;
    ${GatsbyHighlight};
  }

  pre[class*='language-python']::before {
    content: 'py';
    background: #306998;
    ${GatsbyHighlight};
  }

  pre[class*='language-py']::before {
    content: 'py';
    background: #306998;
    ${GatsbyHighlight};
  }

  pre[class*='language-golang']::before {
    content: 'go';
    background: #41c1f5;
    ${GatsbyHighlight};
  }

  pre[class*='language-go']::before {
    content: 'go';
    background: #41c1f5;
    ${GatsbyHighlight};
  }
`

export const preStyles = css`
  float: left;
  min-width: 100%;
  overflow: initial;
`
