import { createGlobalStyle } from 'styled-components'

const GlobalStyle = createGlobalStyle`
  html, body {
    margin: 0;
    padding: 0;
  }

  ${() => {
    return null
  }}

pre {
    position: relative;
    background-color: #061526 !important;
    border-radius: 4px;
    font-size: 14px;
    border-left: 0.3em solid #d23669;
    display: block;
    line-height: 1.5rem;
  }



  .highlight-line {
    padding: 0 1.2rem;
    background-color: rgba(201, 167, 255, 0.2);
    margin: 0 -21px;
    border-left: 2.5px solid #ffa7c4;
  }

  .gatsby-highlight-code-line {
    background-color: #feb;
    display: block;
    margin-right: -1.3125rem;
    margin-left: -1.3125rem;
    padding-right: 1em;
    padding-left: 0.75em;
    border-left: 0.3em solid #f99;
  }

  .gatsby-highlight {
    margin-bottom: 1.75rem;
    margin-left: -1.3125rem;
    margin-right: -1.3125rem;
    border-radius: 10px;
    background: #011627;
    -webkit-overflow-scrolling: touch;
    overflow: auto;
  }

`

export default GlobalStyle
