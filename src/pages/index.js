import React from 'react'
import { graphql, Link } from 'gatsby'
import Bounce from 'react-reveal/Bounce'
import Slide from 'react-reveal/Slide'
import Zoom from 'react-reveal/Zoom'
import styled from 'styled-components'
import Head from '../components/Head'
import Bio from '../components/Bio'
import Footer from '../components/Footer'
import Layout from '../components/Layout'
import SEO from '../components/seo'
import { formatReadingTime } from '../utils/helpers'
import { rhythm, scale } from '../utils/typography'

const StyledSideLeft = styled(Slide)``

class BlogIndex extends React.Component {
  render() {
    const { data } = this.props
    const siteTitle = data.site.siteMetadata.title
    const posts = data.allMdx.edges

    return (
      <Layout location={this.props.location} title={siteTitle}>
        <Head />
        <SEO
          title="Alfie Qashwa"
          keywords={[`javascript`, `react`, `gatsby`, `blog`]}
        />
        <Zoom>
          <aside>
            <Bio />
          </aside>
        </Zoom>
        <main>
          {posts.map(({ node }) => {
            const title = node.frontmatter.title || node.fields.slug
            return (
              <article key={node.fields.slug}>
                <header>
                  <h3
                    style={{
                      fontFamily: 'Montserrat, sans-serif',
                      fontSize: rhythm(1),
                      marginBottom: rhythm(1 / 4),
                    }}
                  >
                    <Link
                      style={{ boxShadow: `none` }}
                      to={node.fields.slug}
                      rel="bookmark"
                    >
                      <Slide right>{title}</Slide>
                    </Link>
                  </h3>
                  <Slide left>
                    <p
                      style={{
                        ...scale(-1 / 5),
                        display: `block`,
                        marginBottom: rhythm(1),
                        marginTop: rhythm(-1 / 5),
                        color: '#f7786b',
                      }}
                    >
                      {/* {formatPostDate(node.frontmatter.date)} */}
                      {node.frontmatter.date}
                      {` • ${formatReadingTime(node.timeToRead)}`}
                    </p>
                  </Slide>
                </header>
                <Slide bottom>
                  <p dangerouslySetInnerHTML={{ __html: node.excerpt }} />
                </Slide>
              </article>
            )
          })}
        </main>
        <Bounce top>
          <Footer />
        </Bounce>
      </Layout>
    )
  }
}

export default BlogIndex

export const pageQuery = graphql`
  query {
    site {
      siteMetadata {
        title
      }
    }
    allMdx(sort: { fields: [frontmatter___date], order: DESC }) {
      edges {
        node {
          excerpt
          fields {
            slug
          }
          timeToRead
          frontmatter {
            date(formatString: "MMMM DD, YYYY")
            title
            # spoiler
          }
        }
      }
    }
  }
`
