import React from 'react'
import kebab from '../../content/assets/kebab_logo.png'
import sabyan from '../../content/assets/sabyan_logo.png'
import twinhouse from '../../content/assets/twinhouse_logo.png'
import Layout from '../components/Layout'

class Resume extends React.Component {
  render() {
    return (
      <Layout location={this.props.location}>
        <main>
          <h1>Resume</h1>
          <h2>Experience</h2>
          <ul>
            <li>
              IT at PT.Sumalindo.Tbk Danamon Build Mega Kuningan from 2006 ~
              2008
            </li>
            <li>IT @ several Karaoke (Inul, Princesa) from 2008 ~ 2011</li>
            <li>Marketing Freelance until 2015</li>
            <li>Develop & Maintain ERP Project 2016 ~ Present</li>
            <li>Web Development 2016 ~ Present</li>
          </ul>
          <h2>Education</h2>
          <ul>
            <li>
              System Information @ Bina Nusantara University
              <ul>
                <li>1999 — Dropped Out ¯(°_o)/¯</li>
              </ul>
            </li>
          </ul>
          <h2>Projects</h2>
          <ul>
            <li>Odoo Enterprise Resourcing Planning</li>
            <li>Build some website (SSG)</li>
          </ul>
          <h2>Skills </h2>
          <ul>
            <li>
              <strong>JavaScript</strong>
              <ul>
                <li>ES2015+</li>
                <li>ReactJS</li>
              </ul>
            </li>
            <li>
              <strong>IDE</strong>
              <ul>
                <li>Vim - NeoVim</li>
                <li>VS Code</li>
              </ul>
            </li>
            <li>
              <strong>Git</strong>
            </li>
            <li>
              <strong>GraphQL</strong>
            </li>
            <li>
              <strong>Apollo Client - on progress</strong>
            </li>
            <li>
              <strong>Prisma - on progress</strong>
            </li>
            <li>
              <strong>OS</strong>
              <ul>
                <li>Ubuntu</li>
                <li>Raspbian</li>
              </ul>
            </li>
            <li>
              <strong>Python - Noobs</strong>
            </li>
            <li>
              <strong>Golang - Noobs</strong>
            </li>
            <li>
              <strong>Styled-components</strong>
            </li>
          </ul>
          <h2>Portfolios</h2>
          <h3>Twin House Coffee & Kitchen</h3>
          <iframe
            width="560"
            height="315"
            src={twinhouse}
            frameborder="0"
            allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
            allowFullscreen
          />
          <ul>
            <li>
              <strong>What I did</strong>
              <ul>
                <li>Develop Odoo V9.0 Project</li>
                <li>Implement Point of Sales for 2 Outlets</li>
                <li>Implement Purchase & Inventory Management</li>
                <li>Training 4 employees</li>
              </ul>
            </li>
            <li>
              <strong>Libraries / Tools</strong>
              <ul>
                <li>Python 2.7</li>
                <li>XML</li>
                <li>Linux Debian</li>
                <li>Raspberry Pi 3</li>
              </ul>
            </li>
            <a href="http://pos.twinhouse-cipete.com/web/login" target="_blank">
              Go to Website →
            </a>
          </ul>
          <h3>Kebab Alibaba Franchise</h3>
          <iframe
            width="560"
            height="315"
            src={kebab}
            frameborder="0"
            allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
            allowFullscreen
          />
          <ul>
            <li>
              <strong>What I did</strong>
              <ul>
                <li>Develop Odoo V10.0 Project</li>
                <li>Implement Point of Sales for 20 Outlets</li>
                <li>Implement 8 Inventories</li>
                <li>Implement Bill of Material</li>
                <li>Implement Purchasing Management</li>
                <li>Implement Accounting System</li>
                <li>Training 5 employees</li>
              </ul>
            </li>
            <li>
              <strong>Libraries / Tools</strong>
              <ul>
                <li>Python 2.7</li>
                <li>XML</li>
                <li>Docker</li>
                <li>Linux Debian</li>
                <li>Raspberry Pi 3</li>
              </ul>
            </li>
            <a href="http://alibabakebab.co/web/login" target="_blank">
              Go to Website →
            </a>
          </ul>
          <h3>Sabyan Band Management</h3>
          <iframe
            width="560"
            height="315"
            src={sabyan}
            frameborder="0"
            allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
            allowFullscreen
          />
          <ul>
            <li>
              <strong>What I did</strong>
              <ul>
                <li>Develop Odoo V10.0 Project</li>
                <li>Implement Accounting & Management System</li>
                <li>Training 1 staff</li>
              </ul>
            </li>
            <li>
              <strong>Libraries / Tools</strong>
              <ul>
                <li>Python 2.7</li>
                <li>XML</li>
                <li>Docker</li>
                <li>Linux Debian</li>
              </ul>
            </li>
            <a href="" target="_blank">
              The Site link is secret →
            </a>
          </ul>
        </main>
      </Layout>
    )
  }
}

export default Resume
