import React from 'react'
import { Link, graphql } from 'gatsby'
import { MDXRenderer } from 'gatsby-plugin-mdx'

import Flip from 'react-reveal/Flip'
import Zoom from 'react-reveal/Zoom'
import Slide from 'react-reveal/Slide'

import '../fonts/fonts-post.css'
import Bio from '../components/Bio'
import Layout from '../components/Layout'
import SEO from '../components/seo'
import { formatReadingTime } from '../utils/helpers'
import { rhythm, scale } from '../utils/typography'

class BlogPostTemplate extends React.Component {
  render() {
    const post = this.props.data.mdx
    const siteTitle = this.props.data.site.siteMetadata.title
    const { previous, next } = this.props.pageContext

    return (
      <Layout location={this.props.location} title={siteTitle}>
        <SEO title={post.frontmatter.title} description={post.excerpt} />
        <main>
          <article>
            <header>
              <Flip>
                <h1 style={{ color: 'var(--textTitle)' }}>
                  {post.frontmatter.title}
                </h1>
              </Flip>
              <Zoom>
                <p
                  style={{
                    ...scale(-1 / 5),
                    display: `block`,
                    marginBottom: rhythm(1),
                    marginTop: rhythm(-4 / 5),
                    color: '#f7786b',
                  }}
                >
                  {post.frontmatter.date}
                  {` • ${formatReadingTime(post.timeToRead)}`}
                </p>
              </Zoom>
            </header>
            <MDXRenderer>{post.body}</MDXRenderer>
            <hr
              style={{
                marginBottom: rhythm(1),
              }}
            />
          </article>
        </main>
        <aside>
          <h3
            style={{
              fontFamily: 'Montserrat, sans-serif',
              marginTop: rhythm(0.25),
            }}
          >
            <Link
              style={{
                boxShadow: 'none',
                textDecoration: 'none',
                color: 'var(--pink)',
              }}
              to={'/'}
            >
              <Flip>Celloworld</Flip>
            </Link>
          </h3>
          <Zoom>
            <Bio />
          </Zoom>
          <nav>
            <ul
              style={{
                display: `flex`,
                flexWrap: `wrap`,
                justifyContent: `space-between`,
                listStyle: `none`,
                padding: 0,
              }}
            >
              <Slide right>
                <li>
                  {previous && (
                    <Link style={{}} to={previous.fields.slug} rel="prev">
                      ← {previous.frontmatter.title}
                    </Link>
                  )}
                </li>
              </Slide>
              <Slide left>
                <li>
                  {next && (
                    <Link to={next.fields.slug} rel="next">
                      {next.frontmatter.title} →
                    </Link>
                  )}
                </li>
              </Slide>
            </ul>
          </nav>
        </aside>
      </Layout>
    )
  }
}

export default BlogPostTemplate

export const pageQuery = graphql`
  query($slug: String!) {
    site {
      siteMetadata {
        title
        author
      }
    }
    mdx(fields: { slug: { eq: $slug } }) {
      id
      timeToRead
      excerpt(pruneLength: 160)
      frontmatter {
        title
        date(formatString: "MMMM DD, YYYY")
      }
      body
    }
  }
`
