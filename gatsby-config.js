module.exports = {
  siteMetadata: {
    title: `Celloworld`,
    author: `Alfie Qashwa`,
    description: `Personal Blog by Alfie Qashwa to Learning In Public`,
    siteUrl: `https://www.alfieqashwa.me`,
    social: {
      twitter: `alfieqashwa`,
    },
  },
  plugins: [
    {
      resolve: `gatsby-source-filesystem`,
      options: {
        path: `${__dirname}/content/blog`,
        name: `blog`,
      },
    },
    {
      resolve: `gatsby-source-filesystem`,
      options: {
        path: `${__dirname}/content/assets`,
        name: `assets`,
      },
    },
    {
      resolve: `gatsby-plugin-mdx`,
      options: {
        extensions: ['.mdx', '.md'],
        defaultLayout: require.resolve('./src/components/Layout.js'),
        gatsbyRemarkPlugins: [
          {
            resolve: 'gatsby-remark-external-links',
            options: {
              target: '_blank',
              rel: 'nofollow noopener noreferrer',
            },
          },
          {
            resolve: `gatsby-remark-images`,
            options: {
              maxWidth: 1035,
              // sizeByPixelDensity: true,
            },
          },
          // },
          // {
          //   resolve: 'gatsby-remark-prismjs',
          //   options: {
          //     classPrefix: 'language-',
          //     inlineCodeMarker: '÷',
          //   },
          // },
          // `gatsby-remark-copy-linked-files`,
          // `gatsby-remark-smartypants`,
          // `typography-plugin-code`,
        ],
        plugins: [`gatsby-remark-images`, `gatsby-remark-autolink-headers`],
      },
    },
    // {
    //   resolve: `gatsby-remark-responsive-iframe`,
    //   options: {
    //     wrapperStyle: `margin-bottom: 1.0725rem`,
    //   },
    // },
    `gatsby-plugin-sharp`,
    `gatsby-transformer-sharp`,
    `gatsby-plugin-styled-components`,
    `gatsby-plugin-polished`,
    `gatsby-plugin-catch-links`,
    `gatsby-plugin-react-helmet`,
    `gatsby-plugin-playground`,
    {
      resolve: `gatsby-plugin-google-analytics`,
      options: {
        trackingId: `UA-134432059-1`,
      },
    },
    {
      resolve: `gatsby-plugin-feed`,
      options: {
        query: `
          {
            site {
              siteMetadata {
                title
                description
                siteUrl
              }
            }
          }
        `,
        feeds: [
          {
            serialize: ({ query: { site, allMdx } }) => {
              return allMdx.edges.map(edge => {
                return Object.assign({}, edge.node.frontmatter, {
                  description: edge.node.excerpt,
                  data: edge.node.frontmatter.date,
                  url: site.siteMetadata.siteUrl + edge.node.fields.slug,
                  guid: site.siteMetadata.siteUrl + edge.node.fields.slug,
                  custom_elements: [{ 'content:encoded': edge.node.boy }],
                })
              })
            },

            /* if you want to filter for only published posts, you can do
             * something like this:
             * filter: { frontmatter: { published: { ne: false } } }
             * just make sure to add a published frontmatter field to all posts,
             * otherwise gatsby will complain
             **/
            query: `
            {
              allMdx(
                limit: 1000,
                sort: { order: DESC, fields: [frontmatter___date] }) {
                edges {
                  node {
                    body
                    fields { slug }
                    frontmatter {
                      title
                      date
                    }
                  }
                }
              }
            }
            `,
            output: '/rss.xml',
            title: 'Alfie Qashwa Celloworld RSS feed',
          },
        ],
      },
    },
    {
      resolve: `gatsby-plugin-manifest`,
      options: {
        name: `Celloworld`,
        short_name: `Celloworld`,
        description: `Learning In Public As Dead As Leaves`,
        start_url: `/`,
        background_color: `#ffffff`,
        // theme_color: `#663399`,
        theme_color: `#ffa7c4`,
        display: `minimal-ui`,
        icon: `content/assets/celloworld-icon.png`,
        crossOrigin: `use-credentials`, // `use-credentials` or `anonymous`
        icons: [
          {
            src: `/android-chrome-192x192.png`,
            sizes: '192x192',
            type: 'image/png',
          },
          {
            src: `/android-chrome-512x512.png`,
            sizes: '512x512',
            type: 'image/png',
          },
          {
            src: `/safari-pinned-tab.svg`,
            sizes: '1118x1118',
            type: 'image/svg',
          },
          {
            src: `/apple-touch-icon.png`,
            sizes: '180x180',
            type: 'image/png',
          },
          {
            src: `/mstile-150x150.png`,
            sizes: '270x270',
            type: 'image/png',
          },
        ],
        theme_color_in_head: false,
      },
    },
    `gatsby-plugin-offline`,
    {
      resolve: `gatsby-plugin-typography`,
      options: {
        pathToConfigModule: `src/utils/typography`,
      },
    },
    {
      resolve: 'gatsby-plugin-i18n',
      options: {
        langKeyDefault: 'en',
        useLangKeyLayout: false,
      },
    },
  ],
}
